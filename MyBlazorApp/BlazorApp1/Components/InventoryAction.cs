﻿using BlazorApp1.Model;

namespace BlazorApp1.Components
{
    public class InventoryAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public SimpleItem Item { get; set; }
    }
}
