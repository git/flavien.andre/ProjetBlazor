﻿using BlazorApp1.Model;
using Microsoft.AspNetCore.Components;
using System.Collections.ObjectModel;

namespace BlazorApp1.Components
{
    public partial class InventoryBars
    {
        [Parameter]
        public List<SimpleItem> Items { get; set; }

        public ObservableCollection<InventoryAction> Actions { get; set; }

        public SimpleItem CurrentDragItem { get; set; }
    }
}
