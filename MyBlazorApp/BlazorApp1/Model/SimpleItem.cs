﻿namespace BlazorApp1.Model
{
    public class SimpleItem
    {
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public int StackSize { get; set; }
    }
}