﻿using BlazorApp1.Components;
using BlazorApp1.Model;
using BlazorApp1.Services;
using Microsoft.AspNetCore.Components;

namespace BlazorApp1.Pages
{
    public partial class Inventory
    {
        public List<SimpleItem> Items { get; set; } = new List<SimpleItem>();

        public IDataService DataService { get; set; }

        protected async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            /*Items = await DataService.List(0, await DataService.Count());*/

            StateHasChanged();
        }
    }
}
